# recent ideas blockchains

- [privacy & ppos](https://eprint.iacr.org/2018/1105.pdf)
    - AVRF (VRF indicate wealth by noticing how often one wins a lottery)
    - rewards leak identity (use ZCash to set anonymous chans for receiving rewards)
    - Q: why should wealth be hidden? how does this go with transperancy?

- [reparo](https://arxiv.org/pdf/2001.00486.pdf)
    - "a generic protocol that acts as a publicly verifiable layer on top of any blockchain to perform repairs, ranging from fixing buggy contracts to removing illicit contents from the chain"