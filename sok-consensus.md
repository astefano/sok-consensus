# Consensus 

## New network models

Why? more realistic? better suited for tolerating more than n/3  byz?

- [intermittently sync](https://eprint.iacr.org/2016/199.pdf)
    - strictly weaker than weak synchrony;
    - a ∆-intermittently synchronous network approximates a ∆-synchronous network in the sense that on average it delivers messages at a rate of 1/∆. However, the delivery rate may be unevenly distributed in time (e.g., “bursty”), delivering no messages at all during some time intervals and delivering messages rapidly during others
- [χ-weakly-synchronous](https://eprint.iacr.org/2019/179.pdf)
    - In every round, more than χ fraction nodes are not only honest but also online; however, the set of honest and online nodes in adjacent rounds need not be the same
    - a “best-effort” notion of liveness:  a protocol has T-liveness iff for any honest node that becomes online in some round r≥T, it must have produced output by the end of round r
    - shows that a BA exists for .5-weakly-sync; seems to be closer to [abraham et al.](https://arxiv.org/abs/1704.02397) but has a "complain" rule which makes it live under .5-weakly-sync 

## Surveys
- ['19](https://arxiv.org/pdf/1904.04098v1.pdf)
- ['17, revisited '19](https://smeiklej.com/files/aft19a.pdf)
- ['18, garay, revisitied '20](https://eprint.iacr.org/2018/754.pdf)

## Finality gadgets
- [near-proto1](https://nearprotocol.com/downloads/PoST.pdf)
- [afgjort](https://eprint.iacr.org/2019/504)
    - 1/3 (partial sync) but 
- [thunderella](https://eprint.iacr.org/2017/913.pdf)
    - tolerates up to 1/2 corruption + responsiveness in the optimistic case
    - voting on the fast path, if no progress fall back on slow path = blockchain (nakamoto for permissioneless, dolev-strong variant for permissioned)
    - proposal for committee election for pos
    - handles adaptive adversary; offline nodes are not byz
    
   
## Async algos

- [BEAT](https://www.csee.umbc.edu/~hbzhang/files/beat.pdf)
    - argues that the use of threshold crypto in [Honeybadger](https://eprint.iacr.org/2016/199.pdf) leads to higher latency and lower throughput (mostly because of threshold crypto)  

## Partial sync algos
- [ibft2](https://arxiv.org/pdf/1909.10194.pdf)
    - dynamic committees, PoA
- [dbft](https://arxiv.org/pdf/1702.03068v3.pdf)
    - consortium, some kind of optimistic responsiveness? (don't be slow because of a slow leader), didactic

## Sync algos
- [comparison](https://decentralizedthoughts.github.io/2019-11-11-authenticated-synchronous-bft/)

## Sharding
- [elastico](https://www.comp.nus.edu.sg/~prateeks/papers/Elastico.pdf)
- [rapidchain](https://dl.acm.org/doi/pdf/10.1145/3243734.3243853?download=true)
- [ostraka](https://arxiv.org/abs/1907.03331)
- 

## DAGs
- [dagmania, danezis'18](https://arxiv.org/pdf/1809.01620.pdf)

## Attacks
- [BDoS PoW](https://arxiv.org/abs/1907.03331)
- 
## New tools for reasoning about consensus, use cases

- [scp in ivy & tla+](https://github.com/stellar/scp-proofs)


## Misc
- [sciencedirect consensus](https://www.sciencedirect.com/topics/computer-science/consensus-algorithm)
- [proposal_standards](http://derekhsorensen.com/docs/Establishing_Standards_for_Consensus_on_Blockchains.pdf)
- 