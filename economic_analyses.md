# Survey of economic analyses of blockchains

References:
- [bitcoin](https://eprint.iacr.org/2018/138.pdf) 
- [Fantomete](https://arxiv.org/pdf/1805.06786.pdf)
- [BFT games](https://arxiv.org/abs/1902.07895)
- [collusion](https://vitalik.ca/general/2019/04/03/collusion.html)
- [bribing attacks-PoS](https://blog.ethereum.org/2016/12/07/history-casper-chapter-2/)
- [bribing attacks-PoW](https://fc18.ifca.ai/bitcoin/papers/bitcoin18-final14.pdf)
- [SoK: Tools for Game Theoretic Models of Security for Cryptocurrencies](https://arxiv.org/abs/1905.08595)
- [incentives in security protocols](https://www.benthamsgaze.org/wp-content/uploads/2018/03/spw18.pdf)
- [fairledger](https://dahliamalkhi.files.wordpress.com/2019/11/fairledger-opodis2019.pdf)


Questions:
- bribing in existing protos?
- isn't there a conflict between 1. & 6.?

Ideas, misc:
- [arthur's perpetuities](https://forum.tezosagora.org/t/perpetuities-as-block-rewards/1303)
- [lower self bond](https://forum.tezosagora.org/t/lower-self-bond-requirement-to-4/1585)
- [taxation rewards](https://forum.tezosagora.org/t/cryptocurrency-economics-and-the-taxation-of-block-rewards/1463)